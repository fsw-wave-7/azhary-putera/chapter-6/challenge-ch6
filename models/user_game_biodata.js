'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User_game_biodata.init({
    name: DataTypes.STRING,
    age: DataTypes.INTEGER,
    email: DataTypes.STRING,
    city: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {
    
    sequelize,
    modelName: 'User_game_biodata',
  });

  User_game_biodata.associate = function(models) {
    User_game_biodata.belongsTo(models.User_game, {foreignKey: 'userId', as: 'user'})
  };

  

  return User_game_biodata;
};