const { User_game, User_game_biodata } = require('../models')
const { Op } = require("sequelize")


class UserController {
    getUser = (req, res) => {
        const name = req.query.name
        let whereCondition
        if (name) {
            whereCondition = {
                where: {
                    name: {
                        [Op.iLike]: `%${name}%`
                    }
                }
            }
        }
        User_game_biodata.findAll(whereCondition)
        .then(user => {
            res.result(user)
        })


        // SELECT * FROM users
    }

    getDetailUser = (req, res) => {
        User_game.findOne({
            where: { id: req.params.id }
          })
          .then(user => {
            res.result(user)

        })

    }

    insertUser = (req, res) => {
        User_game.create({
            username: req.body.username,
            password: req.body.password,
            User_game_biodata: {
                name: req.body.name,
                age: req.body.age,
                email: req.body.email,
                city: req.body.city, 
            }
        })
        .then(user => {
            res.result(user)
        }) .catch(err => {
            res.status(422).json("Can't create user")
        })

    }

    updateUser = (req, res) => {
        User_game.update({
            username: req.body.username,
            password: req.body.password,
            name: req.body.name,
            age: req.body.age,
            email: req.body.email,
            city: req.body.city,
        }, {
        where: { id: req.params.id },
        include: 'User_game_biodata'
        })
        .then(user => {
            User_game.findOne({
                where: { id: req.params.id }
              })
              .then(user => {
                res.result(user)
    
            })

        }) .catch(err => {
            res.status(422).json("Can't update user")
        })

    }

    deleteUser = (req, res) => {
        User_game.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(() => {
            res.result({})
        })

    }
}

module.exports = UserController
