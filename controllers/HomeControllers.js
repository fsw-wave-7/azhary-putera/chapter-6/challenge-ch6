const { join } = require('path')
const {User_game} = require('../models')
const bcrypt = require('bcrypt')

class HomeController {

    index = (req, res) =>{
        User_game.findAll()
        // include: user_game_biodata
        .then(user => {   
        res.render(join(__dirname, '../../views/index'),{
        content: './pages/userlist',
        user : user
            })
        })
    }

    add = (req, res)=>{
        res.render(join(__dirname, '../../views/index'),{
        content: './pages/adduser'
    })
    }
    
    saveUser = async (req, res)=> {
        const salt =await bcrypt.genSalt(10)

        User_game.create({
            
            username: req.body.username,
            password: await bcrypt.hash(req.body.password, salt),
            User_game_biodata: {
                name: req.body.name,
                age: req.body.age,
                email: req.body.email,
                city: req.body.city, 
            }

        })
        .then(() => {
            res.redirect('/')
        }) .catch(err => {
            console.log(err)
        })
    }

    editUser = (req, res) => {
        
        const index = req.params.id;

            User_game.findOne({
                where: { id: index },
                include: 'user_game_biodata'
              })
              .then((user) => {
                res.render(join(__dirname, '../../views/index'),{
                content: './pages/editUser',
                user : user,
            })

        }) .catch(err => {
            console.log(err)
        })  
    }

    updateUser = (req, res) => {
    User_game.update({
            username: req.body.username,
            password: req.body.password,
            name: req.body.name,
            age: req.body.age,
            email: req.body.email,
            city: req.body.city, 
        }, {
        where: { id: req.params.id }
        })
        .then(user => {
            res.redirect('/')
        }) .catch(err => {
            res.status(422).json("Can't update user")
        })

    }

    // deleteUser =
    deleteUser = (req, res) => {
        User_game.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(() => {
            res.redirect('/')
        }) 

    }
    
}

module.exports = HomeController
